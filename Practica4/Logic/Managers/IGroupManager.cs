﻿using System;
using System.Collections.Generic;
using System.Text;

using UPB.Practica4.Logic.Models;

namespace UPB.Practica4.Logic.Managers
{
    public interface IGroupManager
    {
        List<Group> GetAllGroups();
        Group CreateGroup(Group group);
        Group UpdateGroup(Group group);
        Group DeleteGroup(Group group);
    }
}
