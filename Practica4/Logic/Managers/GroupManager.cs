﻿using System;
using System.Collections.Generic;
using System.Text;

using UPB.Practica4.Data;
using UPB.Practica4.Logic.Models;

namespace UPB.Practica4.Logic.Managers
{
    public class GroupManager : IGroupManager
    {
        private int _Id;
        private readonly IDbContext _dbContext;

        public GroupManager(IDbContext dbContext)
        {
            _Id = 0;
            _dbContext = dbContext;
        }

        public List<Group> GetAllGroups()
        {
            return Mappers.DAO_To_DTO_Mapper(_dbContext.GetAll());
        }

        public Group CreateGroup(Group group)
        {
            if (not_Valid(group))
                throw new Exception();

            group.Id = $"Group-{fill(_Id)}";
            _Id++;

            List<Data.Models.Group> groupDAO = Mappers.DTO_To_DAO_Mapper(new List<Group> { group });
            _dbContext.AddGroups(groupDAO);

            return group;
        }

        public Group UpdateGroup(Group group)
        {
            if (not_Valid(group))
                throw new Exception();

            List<Data.Models.Group> groupDAO = Mappers.DTO_To_DAO_Mapper(new List<Group>() { group });
            _dbContext.UpdateGroup(groupDAO);

            return group;
        }

        public Group DeleteGroup(Group group)
        {
            if (not_Valid(group))
                throw new Exception();

            List<Data.Models.Group> groupDAO = Mappers.DTO_To_DAO_Mapper(new List<Group> { group });
            _dbContext.DeleteGroup(groupDAO);

            return group;
        }

        private bool not_Valid(Group group)
        {
            return String.IsNullOrEmpty(group.Name) || group.Name.Length > 50 || group.AvailableSlots <= 0;
        }

        private string fill(int id)
        {
            if (id < 10)
                return $"00{id}";
            else if (id < 100)
                return $"0{id}";
            else
                return $"{id}";
        }
    }

}
