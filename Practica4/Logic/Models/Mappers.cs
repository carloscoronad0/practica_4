﻿using System;
using System.Collections.Generic;

namespace UPB.Practica4.Logic.Models
{
    public static class Mappers
    {
        public static List<Data.Models.Group> DTO_To_DAO_Mapper(List<Group> DTOGroups)
        {
            List<Data.Models.Group> DAOGroups = new List<Data.Models.Group>();

            foreach(Group group in DTOGroups)
            {
                DAOGroups.Add(new Data.Models.Group()
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                });
            }

            return DAOGroups;
        }

        public static List<Group> DAO_To_DTO_Mapper(List<Data.Models.Group> DAOGroups)
        {
            List<Group> DTOGroups = new List<Group>();

            foreach(Data.Models.Group group in DAOGroups)
            {
                DTOGroups.Add(new Group()
                {
                    Id = group.Id,
                    Name = group.Name,
                    AvailableSlots = group.AvailableSlots
                });
            }

            return DTOGroups;
        }
    }
}
