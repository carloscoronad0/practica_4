﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;

using UPB.Practica4.Logic.Models;
using UPB.Practica4.Logic.Managers;

namespace Practica4.Controllers
{
    [ApiController]
    [Route("/api/group")]
    public class GroupController : ControllerBase
    {
        private readonly IGroupManager _groupManager;
        private readonly IConfiguration _config;

        public GroupController(IConfiguration config, IGroupManager groupManager)
        {
            _groupManager = groupManager;
            _config = config;

            Console.Out.WriteLine($"Project: {_config.GetSection("ProjectTitle").Value} \n" +
                $"Opening from: {_config.GetSection("EnvironmentName").Value}");
        }

        [HttpGet]
        public List<Group> GetGroups()
        {
            return _groupManager.GetAllGroups();
        }

        [HttpPost]
        public Group CreateGroup([FromBody] Group group)
        {
            return _groupManager.CreateGroup(group);
        }

        [HttpPut]
        public Group UpdateGroups([FromBody] Group group)
        {
            return _groupManager.UpdateGroup(group);
        }

        [HttpDelete]
        public Group DeleteGroups([FromBody] Group group)
        {
            return _groupManager.DeleteGroup(group);
        }
    }
}
