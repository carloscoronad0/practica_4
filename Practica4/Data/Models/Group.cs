﻿using System;

namespace UPB.Practica4.Data.Models
{
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int AvailableSlots { get; set; }
    }
}
