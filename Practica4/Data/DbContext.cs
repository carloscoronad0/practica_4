﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practica4.Data.Models;

namespace UPB.Practica4.Data
{
    public class DbContext : IDbContext
    {
        public List<Group> GroupTable { get; set; }

        public DbContext()
        {
            GroupTable = new List<Group>();
        }

        public List<Group> AddGroups(List<Group> group)
        {
            GroupTable.AddRange(group);
            return group;
        }

        public List<Group> UpdateGroup(List<Group> groupToUpdate)
        {
            Group foundGroup;
            foreach (Group groupUpdate in groupToUpdate)
            {
                foundGroup = GroupTable.Find(group => group.Id == groupUpdate.Id);
                foundGroup.Name = groupUpdate.Name;
                foundGroup.AvailableSlots = groupUpdate.AvailableSlots;
            }
            return groupToUpdate;
        }

        public List<Group> DeleteGroup(List<Group> groupToDelete)
        {
            GroupTable.RemoveAll(group => groupToDelete.Exists(g => g.Id == group.Id));
            return groupToDelete;
        }

        public List<Group> GetAll()
        {
            return GroupTable;
        }
    }
}
