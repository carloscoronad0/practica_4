﻿using System;
using System.Collections.Generic;
using System.Text;
using UPB.Practica4.Data.Models;

namespace UPB.Practica4.Data
{
    public interface IDbContext
    {
        List<Group> AddGroups(List<Group> group);
        List<Group> UpdateGroup(List<Group> groupToUpdate);
        List<Group> DeleteGroup(List<Group> groupToDelete);
        List<Group> GetAll();
    }
}
